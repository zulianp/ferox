#ifndef FEROX_MATRIX_HPP
#define FEROX_MATRIX_HPP

#include "ferox_Number.hpp"

namespace ferox {
    template <typename T>
    inline constexpr auto det1(const T &mat) {
        return mat[0];
    }

    template <typename T>
    inline constexpr auto det2(const T &mat) {
        return mat[0] * mat[3] - mat[2] * mat[1];
    }

    template <typename T>
    inline constexpr auto det3(const T &mat) {
        return mat[0] * mat[4] * mat[8] + mat[1] * mat[5] * mat[6] + mat[2] * mat[3] * mat[7] -
               mat[0] * mat[5] * mat[7] - mat[1] * mat[3] * mat[8] - mat[2] * mat[4] * mat[6];
    }

    template <typename T, typename Scalar>
    bool invert3(const T &mat, T &mat_inv, const Scalar &det) {
        using ScalarT = typename ScalarType<Scalar>::Type;

        if (disjunction(det == static_cast<ScalarT>(0))) {
            return false;
        }

        mat_inv[0] = (mat[4] * mat[8] - mat[5] * mat[7]) / det;
        mat_inv[1] = (mat[2] * mat[7] - mat[1] * mat[8]) / det;
        mat_inv[2] = (mat[1] * mat[5] - mat[2] * mat[4]) / det;
        mat_inv[3] = (mat[5] * mat[6] - mat[3] * mat[8]) / det;
        mat_inv[4] = (mat[0] * mat[8] - mat[2] * mat[6]) / det;
        mat_inv[5] = (mat[2] * mat[3] - mat[0] * mat[5]) / det;
        mat_inv[6] = (mat[3] * mat[7] - mat[4] * mat[6]) / det;
        mat_inv[7] = (mat[1] * mat[6] - mat[0] * mat[7]) / det;
        mat_inv[8] = (mat[0] * mat[4] - mat[1] * mat[3]) / det;
        return true;
    }

    template <typename T, int Rows_, int Cols_, typename...>
    class Matrix {
    public:
        static constexpr const int Rows = Rows_;
        static constexpr const int Cols = Cols_;

        using Data = T[Rows * Cols];
        Data data_;

        inline constexpr Data &data() const { return data_; }
        inline Data &data() { return data_; }

        inline friend constexpr T det(const Matrix &m) {
            static_assert(Rows == 3, "only implemented for Rows == 3");
            return det3(m.data_);
        }

        inline void zero() { Zero<T>::apply(data_, Rows * Cols); }

        inline bool inverse(Matrix &inv) const {
            static_assert(Rows == 3, "only implemented for Rows == 3");
            return invert3(data_, inv.data_, det(*this));
        }

        template <typename In, typename Out>
        inline void mt_v(const In &in, Out &out) {
            for (int j = 0; j < Cols; ++j) {
                // auto offset_i = i * Cols;

                auto &val = out[j];
                val = 0.0;

                for (int i = 0; i < Rows; ++i) {
                    int offset_i = (i * Cols);
                    val += data_[offset_i + j] * in[i];
                }
            }
        }

        template <typename In, typename Out>
        inline void m_v(const In &in, Out &out) {
            for (int i = 0; i < Rows; ++i) {
                int offset_i = (i * Cols);

                auto &val = out[i];
                val = 0.0;

                for (int j = 0; j < Cols; ++j) {
                    val += data_[offset_i + j] * in[j];
                }
            }
        }

        template <typename In, typename Out>
        void m_mt(const In &in, Out &out) {
            static const int N = In::Cols;

            out.zero();

            for (int i = 0; i < Rows; ++i) {
                int offset_i = (i * Cols);

                for (int j = 0; j < Cols; ++j) {
                    int offset_ij = offset_i + j;

                    for (int k = 0; k < N; ++k) {
                        int offset_ik = (i * N + k);
                        int offset_kj = (j * N + k);

                        out.data_[offset_ij] += data_[offset_ik] * in.data_[offset_kj];
                    }
                }
            }
        }
    };
}  // namespace ferox

#endif  // FEROX_MATRIX_HPP
