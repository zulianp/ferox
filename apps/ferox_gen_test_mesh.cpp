#include <cassert>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

bool write(const std::string &path, const std::string &data) {
    std::ofstream os;
    os.open(path.c_str());

    if (!os.good()) {
        assert(false);
        return false;
    }

    os << data;
    os.close();
    return true;
}

template <typename T>
bool write(const std::string &path, const std::vector<T> &data) {
    std::ofstream os;
    os.open(path.c_str());

    if (!os.good()) {
        assert(false);
        return false;
    }

    os.write((const char *)&data[0], sizeof(T) * data.size());
    os.close();
    return true;
}

bool mkdir(const std::string &path) {
    struct stat st = {0};

    if (stat(path.c_str(), &st) == -1) {
        mkdir(path.c_str(), 0700);
    }

    return true;
}

int main() {
    std::string header = "HEX8";

    std::vector<int> elem = {// elem 0
                             0,
                             1,
                             2,
                             3,
                             4,
                             5,
                             6,
                             7,
                             // elem 1
                             4,
                             5,
                             6,
                             7,
                             8,
                             9,
                             10,
                             11};

    std::vector<double> xyz = {
        0.0, 0.0, 0.0,  // 0
        1.0, 0.0, 0.0,  // 1
        1.0, 1.0, 0.0,  // 2
        0.0, 1.0, 0.0,  // 3
        0.0, 0.0, 1.0,  // 4
        1.0, 0.0, 1.0,  // 5
        1.0, 1.0, 1.0,  // 6
        0.0, 1.0, 1.0,  // 7
        0.0, 0.0, 2.0,  // 8
        2.0, 0.0, 2.0,  // 9
        2.0, 2.0, 2.0,  // 10
        0.0, 2.0, 2.0,  // 11
    };

    mkdir("test_mesh");

    if (!write("test_mesh/header.txt", header)) {
        return EXIT_FAILURE;
    }

    if (!write("test_mesh/elems.raw", elem)) {
        return EXIT_FAILURE;
    }

    if (!write("test_mesh/points.raw", xyz)) {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
