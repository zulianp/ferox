#include <array>
#include <functional>

#include "ferox_Number.hpp"
#include "ferox_bench_commons.hpp"
#include "ferox_gauss.hpp"

#ifdef FEROX_HAVE_VC
#include "ferox_vc.hpp"
#endif  // FEROX_HAVE_VC

template <typename T>
class Hex8 {
public:
    using Point = ferox::Vector<T, 3>;
    using Vector = ferox::Vector<T, 3>;
    static const int NNodes = 8;

    using Scalar = typename ferox::ScalarType<T>::Type;
    static constexpr Scalar one() { return static_cast<Scalar>(1); }
    static constexpr Scalar zero() { return static_cast<Scalar>(0); }

    class Grad final {
    public:
        Grad() {
            // f = (1.0 - p.x()) * (1.0 - p.y()) * (1.0 - p.z());
            f[0] = [](const Point &p, Vector &g) {
                g.x() = -(one() - p.y()) * (one() - p.z());
                g.y() = -(one() - p.x()) * (one() - p.z());
                g.z() = -(one() - p.x()) * (one() - p.y());
            };

            // f = p.x() * (one() - p.y()) * (one() - p.z());
            f[1] = [](const Point &p, Vector &g) {
                g.x() = (one() - p.y()) * (one() - p.z());
                g.y() = -p.x() * (one() - p.z());
                g.z() = -p.x() * (one() - p.y());
            };

            // f = p.x() * p.y() * (one() - p.z());
            f[2] = [](const Point &p, Vector &g) {
                g.x() = p.y() * (one() - p.z());
                g.y() = p.x() * (one() - p.z());
                g.z() = -p.x() * p.y();
            };

            // f = (one() - p.x()) * p.y() * (one() - p.z());
            f[3] = [](const Point &p, Vector &g) {
                g.x() = -p.y() * (one() - p.z());
                g.y() = (one() - p.x()) * (one() - p.z());
                g.z() = -(one() - p.x()) * p.y();
            };

            // f = (one() - p.x()) * (one() - p.y()) * p.z();
            f[4] = [](const Point &p, Vector &g) {
                g.x() = -(one() - p.y()) * p.z();
                g.y() = -(one() - p.x()) * p.z();
                g.z() = (one() - p.x()) * (one() - p.y());
            };

            // f = p.x() * (one() - p.y()) * p.z();
            f[5] = [](const Point &p, Vector &g) {
                g.x() = (one() - p.y()) * p.z();
                g.y() = -p.x() * p.z();
                g.z() = p.x() * (one() - p.y());
            };

            // f = p.x() * p.y() * p.z();
            f[6] = [](const Point &p, Vector &g) {
                g.x() = p.y() * p.z();
                g.y() = p.x() * p.z();
                g.z() = p.x() * p.y();
            };

            // f = (one() - p.x()) * p.y() * p.z();
            f[7] = [](const Point &p, Vector &g) {
                g.x() = -p.y() * p.z();
                g.y() = (one() - p.x()) * p.z();
                g.z() = (one() - p.x()) * p.y();
            };
        }

        template <typename Scalar>
        inline auto eval(const Point &p, const Scalar *coeff) const {
            Vector g;

            f[0](p, g);

            Vector ret = g * coeff[0];
            for (int i = 1; i < NNodes; ++i) {
                f[i](p, g);
                ret += g * coeff[i];
            }

            return ret;
        }

        std::array<std::function<void(const Point &, Vector &)>, NNodes> f;
    };

    class Fun final {
    public:
        Fun() {
            f[0] = [](const Point &p) -> T { return (one() - p.x()) * (one() - p.y()) * (one() - p.z()); };

            f[1] = [](const Point &p) -> T { return p.x() * (one() - p.y()) * (one() - p.z()); };

            f[2] = [](const Point &p) -> T { return p.x() * p.y() * (one() - p.z()); };

            f[3] = [](const Point &p) -> T { return (one() - p.x()) * p.y() * (one() - p.z()); };

            f[4] = [](const Point &p) -> T { return (one() - p.x()) * (one() - p.y()) * p.z(); };

            f[5] = [](const Point &p) -> T { return p.x() * (one() - p.y()) * p.z(); };

            f[6] = [](const Point &p) -> T { return p.x() * p.y() * p.z(); };

            f[7] = [](const Point &p) -> T { return (one() - p.x()) * p.y() * p.z(); };
        }

        template <typename Scalar>
        inline auto eval(const Point &p, const Scalar *coeff) const {
            auto ret = f[0](p) * coeff[0];
            for (int i = 1; i < NNodes; ++i) {
                ret += f[i](p) * coeff[i];
            }

            return ret;
        }

        std::array<std::function<T(const Point &)>, NNodes> f;
    };

    class Jacobian {
    public:
        static const int Dim = 3;
        static const int PhysicalDim = 3;

        template <typename Node, typename Jac>
        inline static auto eval(const Point &p, const Node *nodes, Jac &J) {
            auto &data = J.data();

            Vector g;
            Grad grad;

            grad.f[0](p, g);

            for (int i = 0; i < PhysicalDim; ++i) {
                const int i_offset = i * Dim;

                for (int j = 0; j < Dim; ++j) {
                    const int idx = i_offset + j;
                    data[idx] = nodes[0][i] * g[j];
                }
            }

            for (int k = 1; k < NNodes; ++k) {
                grad.f[k](p, g);

                for (int i = 0; i < PhysicalDim; ++i) {
                    const int i_offset = i * Dim;

                    for (int j = 0; j < Dim; ++j) {
                        const int idx = i_offset + j;
                        data[idx] += nodes[k][i] * g[j];
                    }
                }
            }
        }
    };
};

template <typename C, typename Node, typename T>
auto integrate(const C *coeff, const Node *nodes, const ferox::Quadrature<T, 3> &q) {
    const int N_qp = q.weights.size();

    typename Hex8<T>::Fun fun;
    typename Hex8<T>::Jacobian jacobian;

    ferox::Matrix<T, 3, 3> J;

    jacobian.eval(q.points[0], nodes, J);
    auto integral = ferox::sum(fun.eval(q.points[0], coeff) * q.weights[0] * det(J));

    for (int k = 1; k < N_qp; ++k) {
        jacobian.eval(q.points[k], nodes, J);
        integral += ferox::sum(fun.eval(q.points[k], coeff) * q.weights[k] * det(J));
    }

    return integral;
}

template <typename C, typename Node, typename T>
void laplace(const C *coeff, const Node *nodes, const ferox::Quadrature<T, 3> &q, C *res) {
    const int N_qp = q.weights.size();

    typename Hex8<T>::Grad grad;
    typename Hex8<T>::Jacobian jacobian;

    ferox::Matrix<T, 3, 3> J, J_inv, FF;

    jacobian.eval(q.points[0], nodes, J);
    J.inverse(J_inv);
    // compute the first f
    J_inv.m_mt(J, FF);

    ferox::Vector<T, 3> g_i, g_x_W;
    // The jacobian is missing
    g_i = grad.eval(q.points[0], &coeff[0]) * q.weights[0] * det(J);
    FF.m_v(g_i, g_x_W);

    for (int i = 0; i < 8; ++i) {
        grad.f[i](q.points[0], g_i);
        res[i] = ferox::sum(dot(g_x_W, g_i));
    }

    for (int k = 1; k < N_qp; ++k) {
        jacobian.eval(q.points[k], nodes, J);
        J.inverse(J_inv);

        g_i = grad.eval(q.points[k], coeff) * q.weights[k] * det(J);
        FF.m_v(g_i, g_x_W);

        for (int i = 0; i < 8; ++i) {
            grad.f[i](q.points[k], g_i);
            res[i] += ferox::sum(dot(g_x_W, g_i));
        }
    }
}

template <typename T, typename Scalar = EntryType>
class BM_FEM {
public:
    auto run() {
        EntryType ret = 0.0;

        ferox_parallel_for(std::size_t i = 0; i < n_elements; ++i) {
            // for (std::size_t i = 0; i < n_elements; ++i) {
            laplace(&coeffs[i * 8], &points[i * 8], q, &residual[i * 8]);
            ret += integrate(&coeffs[i * 8], &points[i * 8], q);
        }

        return ret;
    }

    void init() {
        ferox::Gauss<T>::Hex::get(4, q);
        // ferox::Gauss<T>::Hex::get(1, q);

        coeffs.resize(8 * n_elements, 1.0);
        residual.resize(8 * n_elements, 1.0);
        points.resize(8 * n_elements);

        srand(1);

        for (auto &p : points) {
            p.x() = double(rand()) / RAND_MAX;
            p.y() = double(rand()) / RAND_MAX;
            p.z() = double(rand()) / RAND_MAX;
        }
    }

    BM_FEM() { init(); }

    ferox::Quadrature<T, 3> q;
    // 8 Millions
    std::size_t n_elements{8000000};
    // 1 million
    // std::size_t n_elements{1000000};
    // 80 thousands
    // std::size_t n_elements{80000};

    // std::size_t n_elements{10};

    std::vector<Scalar> coeffs, residual;
    std::vector<ferox::Vector<Scalar, 3>> points;
};

static void BM_fem_vanilla(benchmark::State &state) {
    BM_FEM<EntryType> bm_fem;
    for (auto _ : state) {
        auto res = bm_fem.run();
        benchmark::DoNotOptimize(res);
    }
}

BENCHMARK(BM_fem_vanilla);

#ifdef FEROX_HAVE_VC

static void BM_fem_Vc(benchmark::State &state) {
    BM_FEM<Vc::Vector<EntryType>> bm_fem;
    for (auto _ : state) {
        auto res = bm_fem.run();
        benchmark::DoNotOptimize(res);
    }
}

BENCHMARK(BM_fem_Vc);

#endif  // FEROX_HAVE_VC
