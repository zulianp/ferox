#ifndef FEROX_SORT_HPP
#define FEROX_SORT_HPP

#include "ferox_Export.hpp"

#include <mpi.h>

namespace ferox {

    template <typename T>
    class FEROX_EXPORT Sort {
    public:
        static int apply(MPI_Comm comm, int nsend, T *sendbuf, int nrecv, T *recvbuf);
    };

    template <typename T>
    inline int FEROX_EXPORT sort(MPI_Comm comm, int nsend, T *sendbuf, int nrecv, T *recvbuf) {
        return Sort<T>::apply(comm, nsend, sendbuf, nrecv, recvbuf);
    }

}  // namespace ferox

#endif  // FEROX_SORT_HPP