#include "ferox_Sort.hpp"

#include "gtest/gtest.h"

using namespace ferox;

extern "C" {

int MPI_Sort(const void *sendbuf,
             const int sendcount,
             MPI_Datatype datatype,
             const void *recvbuf,
             const int recvcount,
             MPI_Comm comm);

int MPI_Sort_bykey(const void *sendkeys,
                   const void *sendvals,
                   const int sendcount,
                   MPI_Datatype keytype,
                   MPI_Datatype valtype,
                   void *recvkeys,
                   void *recvvals,
                   const int recvcount,
                   MPI_Comm comm);
}

TEST(SortTest, sort_ints) {
    int r, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &r);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int n = 600;
    std::vector<int32_t> values(n);
    std::generate(std::begin(values), std::end(values), []() { return 100 * double(rand()) / RAND_MAX; });
    std::vector<int32_t> keys = values;

    std::vector<int32_t> sorted_values(n, 0);
    std::vector<int32_t> sorted_keys = sorted_values;

    MPI_Sort_bykey(
        &keys[0], &values[0], n, MPI_INT32_T, MPI_INT32_T, &sorted_keys[0], &sorted_values[0], n, MPI_COMM_WORLD);

    EXPECT_TRUE(std::is_sorted(std::begin(sorted_values), std::end(sorted_values)));
}
