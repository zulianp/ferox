
#include "ferox_bench_commons.hpp"

static const int N = 100000;

EntryType reduce_vanilla(const std::vector<EntryType> &data) {
    EntryType ret = 0;

    for (auto d : data) {
        ret += d;
    }

    return ret;
}

static void BM_reduce_vanilla(benchmark::State &state) {
    int n = N;
    std::vector<EntryType> data(n);

    int gen = 0;
    std::generate(std::begin(data), std::end(data), [&]() { return gen++ / double(N); });

    for (auto _ : state) {
        auto res = reduce_vanilla(data);
        benchmark::DoNotOptimize(res);
    }
}
// Register the function as a benchmark
BENCHMARK(BM_reduce_vanilla);

#ifdef FEROX_HAVE_VC

EntryType reduce_vc(const std::vector<EntryType> &data) {
    using VectorType = Vc::Vector<EntryType>;

    EntryType ret = 0;

    const size_t data_size = data.size();
    const size_t size_blocks = (data_size / VectorType::Size) * VectorType::Size;

    size_t k = 0;
    for (; k < size_blocks; k += VectorType::Size) {
        auto v = VectorType(&data[k], Vc::Unaligned);
        ret += v.sum();
    }

    // sanitize
    for (; k < data_size; ++k) {
        ret += data[k];
    }

    return ret;
}

static void BM_reduce_Vc(benchmark::State &state) {
    int n = N;
    std::vector<EntryType> data(n);

    // printf("LANES: %d\n", LANES);

    int gen = 0;
    std::generate(std::begin(data), std::end(data), [&]() { return gen++ / double(N); });

    for (auto _ : state) {
        auto res = reduce_vc(data);
        benchmark::DoNotOptimize(res);
    }
}
// Register the function as a benchmark
BENCHMARK(BM_reduce_Vc);

#endif  // FEROX_HAVE_VC

#ifdef FEROX_HAVE_OPENMP
EntryType reduce_openmp(const std::vector<EntryType> &data) {
    EntryType ret = 0;
    ptrdiff_t n = data.size();

#pragma omp parallel for reduction(+ : ret)
    for (ptrdiff_t i = 0; i < n; ++i) {
        ret += data[i];
    }

    return ret;
}

static void BM_reduce_openmp(benchmark::State &state) {
    int n = N;
    std::vector<EntryType> data(n);

    // #pragma omp parallel
    //   {
    //     // std::cout << "omp_get_num_threads: " << omp_get_num_threads() <<
    //     // std::endl; printf("Hello World... from thread = %d\n",
    //     // omp_get_thread_num());
    //   }

    int gen = 0;
    std::generate(std::begin(data), std::end(data), [&]() { return gen++ / double(N); });

    for (auto _ : state) {
        auto res = reduce_openmp(data);
        benchmark::DoNotOptimize(res);
    }
}
// Register the function as a benchmark
BENCHMARK(BM_reduce_openmp);
#endif  // FEROX_HAVE_OPENMP
