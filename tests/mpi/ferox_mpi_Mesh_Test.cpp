#include "ferox_mpi_Mesh.hpp"

#include "gtest/gtest.h"

using namespace ferox;

TEST(MPI_Mesh_Test, read) {
    Mesh<double, int> mesh;
    mesh.read_raw(MPI_COMM_WORLD, "test_mesh");
    mesh.describe();

    ASSERT_TRUE(mesh.elem_range().begin >= 0);
}
