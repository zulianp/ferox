#ifndef COMMON_HPP
#define COMMON_HPP

#include "ferox_Config.hpp"

#include <iostream>
#include <vector>

#include <benchmark/benchmark.h>

#include "ferox_Config.hpp"

// using EntryType = float;
using EntryType = double;
// using VectorType = Vc::Vector<EntryType>;
// static const int LANES = VectorType::Size;

// template <typename T> struct ScalarType { using Type = T; };
// template <typename T> struct ScalarType<Vc::Vector<T>> { using Type = T; };

#ifdef FEROX_HAVE_OPENMP
#include <omp.h>

#define FEROX_STR(x) #x
#define FEROX_STRINGIFY(x) STR(x)
#define FEROX_CONCATENATE(X, Y) X(Y)

#define ferox_parallel_private(macro_tid_) _Pragma(STRINGIFY(CONCATENATE(omp parallel private, macro_tid_)))

#define ferox_parallel_for _Pragma("omp parallel for") for

#else
#define ferox_parallel_for for
#define ferox_parallel_private(x)

#endif  // FEROX_HAVE_OPENMP

#ifdef FEROX_HAVE_VC
#include <Vc/Vc>
#endif  // FEROX_HAVE_VC

#endif  // COMMON_HPP
