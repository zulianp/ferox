#ifndef FEROX_CUDA_HPP
#define FEROX_CUDA_HPP

#include "ferox_Export.hpp"

namespace ferox {
    void FEROX_EXPORT cuda_info();

}  // namespace ferox

#endif  // FEROX_CUDA_HPP
