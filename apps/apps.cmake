# apps.cmake
set(FEROX_APP_DIR ${CMAKE_CURRENT_SOURCE_DIR}/apps)

add_executable(gen_test_mesh ${FEROX_APP_DIR}/ferox_gen_test_mesh.cpp)
target_compile_features(gen_test_mesh PUBLIC cxx_std_14)
