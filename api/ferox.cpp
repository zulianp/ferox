#include "ferox.hpp"

#include "ferox_Config.hpp"
#include "ferox_Macros.hpp"

#ifdef FEROX_HAVE_MPI
#include <mpi.h>
#endif  // FEROX_HAVE_MPI

namespace ferox {

    void Ferox::init(int argc, char *argv[]) {
#ifdef FEROX_HAVE_MPI
        MPI_Init(&argc, &argv);
#else
        FEROX_UNUSED(argc);
        FEROX_UNUSED(argv);
#endif  // FEROX_HAVE_MPI
    }

    int Ferox::finalize() {
#ifdef FEROX_HAVE_MPI
        return MPI_Finalize();
#else
        return 0;
#endif  // FEROX_HAVE_MPI
    }

}  // namespace ferox