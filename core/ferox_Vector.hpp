#ifndef FEROX_VECTOR_HPP
#define FEROX_VECTOR_HPP

#include "ferox_Number.hpp"

namespace ferox {

    template <typename T, int Dim, typename...>
    class Vector {};

    template <typename T>
    T sum(const T v) {
        return v;
    }

    template <typename T>
    class Vector<T, 3> {
    public:
        T data_[3] = {Zero<T>::make(), Zero<T>::make(), Zero<T>::make()};

        inline T &x() { return data_[0]; }
        inline constexpr const T &x() const { return data_[0]; }

        inline T &y() { return data_[1]; }
        inline constexpr const T &y() const { return data_[1]; }

        inline T &z() { return data_[2]; }
        inline constexpr const T &z() const { return data_[2]; }

        inline constexpr Vector operator*(const T &scale) { return {x() * scale, y() * scale, z() * scale}; }

        inline constexpr const T &operator()(const int idx) const { return data_[idx]; }
        inline T &operator()(const int idx) { return data_[idx]; }

        inline constexpr const T &operator[](const int idx) const { return data_[idx]; }
        inline T &operator[](const int idx) { return data_[idx]; }

        inline constexpr Vector operator+=(const Vector &other) {
            x() += other.x();
            y() += other.y();
            z() += other.z();
            return *this;
        }

        friend inline constexpr T dot(const Vector &l, const Vector &r) {
            return l.x() * r.x() + l.y() * r.y() + l.z() * r.z();
        }
    };

}  // namespace ferox

#endif  // FEROX_VECTOR_HPP
