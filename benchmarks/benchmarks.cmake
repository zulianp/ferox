# benchmarks.cmake

set(FEROX_BENCH_DIR ${CMAKE_SOURCE_DIR}/benchmarks)

set(FEROX_TEST_DIR ${CMAKE_CURRENT_SOURCE_DIR}/benchmarks)

list(APPEND BENCH_MODULES .)

if(FEROX_HAVE_VC)
    list(APPEND BENCH_MODULES vc)
endif()

find_project_files(${FEROX_TEST_DIR} "${BENCH_MODULES}" BENCH_HEADERS
                   BENCH_SOURCES)

add_executable(ferox_bench ${BENCH_SOURCES} ${BENCH_HEADERS})
target_link_libraries(ferox_bench PRIVATE ferox ${FEROX_BENCH_LIBRARIES})
target_include_directories(ferox_bench PRIVATE ${FEROX_BENCH_DIR})
