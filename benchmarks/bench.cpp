#include "ferox.hpp"
#include <benchmark/benchmark.h>

// BENCHMARK_MAIN();

int main(int argc, char **argv) {
  using namespace ferox;

  Ferox::init(argc, argv);

  ::benchmark::Initialize(&argc, argv);
  ::benchmark::RunSpecifiedBenchmarks();

  return Ferox::finalize();
}
