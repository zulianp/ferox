# FeroxDependencies.cmake

include(cmake/FeroxCMakeMacros.cmake)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

# ##############################################################################

set(BENCHMARK_ENABLE_TESTING OFF)

ensure_dependency(benchmark "https://github.com/google/benchmark.git" "v1.5.2")

if(benchmark_FOUND)
    if(benchmark_FETCHED)
        set_target_properties(benchmark PROPERTIES FOLDER extern)
        set_target_properties(benchmark_main PROPERTIES FOLDER extern)

        list(APPEND FEROX_BENCH_LIBRARIES benchmark)

    else()
        list(APPEND FEROX_BENCH_LIBRARIES benchmark::benchmark)
    endif()

else()
    message(FATAL_ERROR "benchmark is required")
endif()

# ##############################################################################

ensure_dependency(GTest "https://github.com/google/googletest.git"
                  "release-1.10.0")

if(GTest_FOUND)
    enable_testing()

    if(GTest_FETCHED)
        set_target_properties(gtest PROPERTIES FOLDER extern)
        set_target_properties(gtest_main PROPERTIES FOLDER extern)
        set_target_properties(gmock PROPERTIES FOLDER extern)
        set_target_properties(gmock_main PROPERTIES FOLDER extern)

        set(FEROX_TEST_LIBRARIES gtest gmock)
    else()
        set(FEROX_TEST_LIBRARIES GTest::GTest)
    endif()

    include(GoogleTest)

    macro(ferox_add_test TESTNAME TEST_SOURCES)
        add_executable(${TESTNAME} ${TEST_SOURCES})
        target_link_libraries(${TESTNAME} ferox ${FEROX_TEST_LIBRARIES})

        gtest_add_tests(TARGET ${TESTNAME} SOURCES ${TEST_SOURCES})
        set_target_properties(${TESTNAME} PROPERTIES FOLDER tests)

        # Does not work on cluster env where you cannot run MPI on master node
        # So for the moment it will only work on apple laptops (the test can be
        # run with ./moonolith_test anyway but not with make test) if(NOT APPLE
        # OR NOT BUILD_SHARED_LIBS) if(APPLE OR WIN32 OR
        # FEROX_ALLOW_DISCOVER_TESTS) gtest_discover_tests( ${TESTNAME}
        # WORKING_DIRECTORY ${PROJECT_DIR} PROPERTIES
        # VS_DEBUGGER_WORKING_DIRECTORY "${PROJECT_DIR}") endif() endif()
    endmacro()

else()
    message(FATAL_ERROR "GTest is required")
endif()

# ##############################################################################

if(FEROX_ENABLE_VC)
    # Unfortunately Vc has to be installed separately and cannot be used as a
    # sub-project
    ensure_dependency(Vc "https://github.com/VcDevel/Vc.git" "1.4.1")

    if(Vc_FOUND)
        if(Vc_AVX_INTRINSICS_BROKEN)
            message(FATAL_ERROR "Vc_AVX_INTRINSICS_BROKEN")
        endif()

        set(FEROX_VC_DEP_LIBRARIES Vc::Vc)
        list(APPEND FEROX_DEP_LIBRARIES Vc::Vc)
        set(FEROX_HAVE_VC TRUE)

        list(APPEND FEROX_VC_DEP_DEFINITIONS "${Vc_ALL_FLAGS}")
        list(APPEND FEROX_DEP_DEFINITIONS "${Vc_ALL_FLAGS}")
        # message(STATUS "FEROX_DEP_DEFINITIONS=${FEROX_DEP_DEFINITIONS}")
    else()
        message(FATAL_ERROR "Vc is required")
    endif()
endif()

# ##############################################################################

if(FEROX_ENABLE_OPENMP)
    find_package(OpenMP REQUIRED)

    if(OpenMP_FOUND)
        set(FEROX_HAVE_OPENMP TRUE)
        list(APPEND FEROX_DEP_LIBRARIES OpenMP::OpenMP_CXX)
    endif()
endif()

# ##############################################################################

if(FEROX_ENABLE_MPI)
    find_package(MPI REQUIRED)

    if(MPI_FOUND)
        set(FEROX_HAVE_MPI TRUE)
        list(APPEND FEROX_DEP_LIBRARIES MPI::MPI_C)
    endif()
endif()

# ##############################################################################

if(FEROX_ENABLE_CUDA)
    include(CheckLanguage)
    check_language(CUDA)
    enable_language(CUDA)
    set(FEROX_HAVE_CUDA TRUE)
    set(CMAKE_CUDA_STANDARD 14)
    set(CMAKE_CUDA_STANDARD_REQUIRED ON)

    # cuda_select_nvcc_arch_flags(ARCH_FLAGS)

    message(
        STATUS
            "CUDA_VERSION=${CUDA_VERSION}, CUDA_TOOLKIT_ROOT_DIR=${CUDA_TOOLKIT_ROOT_DIR}"
    )
endif()

# ##############################################################################

if(FEROX_ENABLE_KOKKOS)
    set(Kokkos_CXX_STANDARD
        14
        CACHE INTERNAL "")

    set(Kokkos_ENABLE_OPENMP
        ${FEROX_HAVE_OPENMP}
        CACHE INTERNAL "")

    set(Kokkos_ENABLE_CUDA
        ${FEROX_HAVE_CUDA}
        CACHE INTERNAL "")

    ensure_dependency(Kokkos "https://github.com/kokkos/kokkos.git" "3.2.00")

    if(Kokkos_FOUND)
        set(FEROX_HAVE_KOKKOS TRUE)

        if(Kokkos_FETCHED)
            # Part of the project
            list(APPEND FEROX_KOKKOS_DEP_LIBRARIES kokkoscore kokkoscontainers)
        else()
            # message("\nFound Kokkos!  Here are the details: ") message("
            # Kokkos_CXX_COMPILER = ${Kokkos_CXX_COMPILER}") message("
            # Kokkos_INCLUDE_DIRS = ${Kokkos_INCLUDE_DIRS}") message("
            # Kokkos_LIBRARIES = ${Kokkos_LIBRARIES}") message("
            # Kokkos_TPL_LIBRARIES = ${Kokkos_TPL_LIBRARIES}") message("
            # Kokkos_LIBRARY_DIRS = ${Kokkos_LIBRARY_DIRS}")
            # message("Kokkos_INCLUDE_DIRS = ${Kokkos_INCLUDE_DIRS}")

            list(APPEND FEROX_KOKKOS_DEP_LIBRARIES ${Kokkos_LIBRARIES}
                 ${Kokkos_TPL_LIBRARIES} ${Kokkos_LIBRARY_DIRS})

            list(APPEND FEROX_KOKKOS_DEP_INCLUDES ${Kokkos_INCLUDE_DIRS})
        endif()

    endif()
endif()

# ##############################################################################

if(FEROX_ENABLE_MPI_SORT)
    find_package(MPISort REQUIRED)

    if(MPISort_FOUND)
        set(FEROX_HAVE_MPI_SORT TRUE)
        list(APPEND FEROX_DEP_LIBRARIES ${MPISort_LIBRARIES})
        list(APPEND FEROX_DEP_INCLUDES ${MPISort_INCLUDE_DIRS})
    else()
        message(FATAL_ERROR "Bad!")
    endif()
endif()

# ##############################################################################
