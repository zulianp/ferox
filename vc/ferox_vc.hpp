#ifndef FEROX_VC_HPP
#define FEROX_VC_HPP

#include "ferox_gauss.hpp"

#include <Vc/Vc>

namespace ferox {

    template <typename T>
    struct ScalarType<Vc::Vector<T>> {
        using Type = T;
    };

    template <typename T>
    struct Zero<Vc::Vector<T>> {
        static void apply(Vc::Vector<T> *data, ptrdiff_t count) {
            for (ptrdiff_t i = 0; i < count; ++i) {
                data[i] = Vc::Vector<T>::Zero();
            }
        }

        inline static Vc::Vector<T> make() { return Vc::Vector<T>::Zero(); }
    };

    template <typename... Args>
    struct Disjunction<Vc::Mask<Args...>> {
        inline constexpr static bool eval(const Vc::Mask<Args...> &v) { return v.count() > 0; }
    };

    template <typename T>
    T sum(const Vc::Vector<T> &v) {
        return v.sum();
    }

    template <typename T, int Dim>
    class Quadrature<Vc::Vector<T>, Dim> {
    public:
        using VecType = Vc::Vector<T>;
        using Point = ferox::Vector<VecType, Dim>;

        Vc::vector<Point> points;
        Vc::vector<VecType> weights;

        void resize(int n) {
            points.resize(n);
            weights.resize(n);
        }
    };

    template <typename T>
    class Gauss<Vc::Vector<T>> {
    public:
        using VectorType = Vc::Vector<T>;
        static const int block_size = VectorType::Size;

        class Hex {
        public:
            static bool get(const int order, Quadrature<VectorType, 3> &q) {
                Quadrature<T, 3> q_scalar;

                if (!Gauss<T>::Hex::get(order, q_scalar)) {
                    return false;
                }

                int n_points = q_scalar.weights.size();
                int n_blocks = n_points / block_size;
                int n_defect = n_points - n_blocks * block_size;

                int n_simd_blocks = n_blocks + (n_defect != 0);

                q.resize(n_simd_blocks);

                std::vector<T> block_w(block_size);
                std::vector<T> block_x(block_size);
                std::vector<T> block_y(block_size);
                std::vector<T> block_z(block_size);

                for (int i = 0; i < n_blocks; ++i) {
                    int q_offset = i * block_size;

                    for (int k = 0; k < block_size; ++k) {
                        int q_idx = q_offset + k;

                        block_x[k] = q_scalar.points[q_idx].x();
                        block_y[k] = q_scalar.points[q_idx].y();
                        block_z[k] = q_scalar.points[q_idx].z();
                        block_w[k] = q_scalar.weights[q_idx];
                    }

                    q.points[i].x() = VectorType(&block_x[0], Vc::Unaligned);
                    q.points[i].y() = VectorType(&block_y[0], Vc::Unaligned);
                    q.points[i].z() = VectorType(&block_z[0], Vc::Unaligned);
                    q.weights[i] = VectorType(&block_w[0], Vc::Unaligned);
                }

                if (n_defect) {
                    // printf("defect %d\n", n_defect);

                    std::fill(block_x.begin(), block_x.end(), 0.0);
                    std::fill(block_y.begin(), block_y.end(), 0.0);
                    std::fill(block_z.begin(), block_z.end(), 0.0);
                    std::fill(block_w.begin(), block_w.end(), 0.0);

                    for (int k = 0; k < n_defect; ++k) {
                        int q_idx = n_blocks * block_size + k;

                        block_x[k] = q_scalar.points[q_idx].x();
                        block_y[k] = q_scalar.points[q_idx].y();
                        block_z[k] = q_scalar.points[q_idx].z();
                        block_w[k] = q_scalar.weights[q_idx];
                    }

                    q.points[n_blocks].x() = VectorType(&block_x[0], Vc::Unaligned);
                    q.points[n_blocks].y() = VectorType(&block_y[0], Vc::Unaligned);
                    q.points[n_blocks].z() = VectorType(&block_z[0], Vc::Unaligned);
                    q.weights[n_blocks] = VectorType(&block_w[0], Vc::Unaligned);
                }

                return true;
            }
        };
    };

}  // namespace ferox

#endif  // FEROX_VC_HPP
