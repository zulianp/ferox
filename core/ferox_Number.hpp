#ifndef FEROX_NUMBER_HPP
#define FEROX_NUMBER_HPP

#include <cstring>

namespace ferox {

    template <typename T>
    struct ScalarType {
        using Type = T;
    };

    template <typename T>
    struct Zero {
        static void apply(T *data, ptrdiff_t count) { std::memset(data, 0, sizeof(T) * count); }
        inline static constexpr T make() { return static_cast<T>(0); }
    };

    template <typename T>
    struct Disjunction {};

    template <>
    struct Disjunction<bool> {
        inline constexpr static bool eval(const bool v) { return v; }
    };

    template <typename T>
    inline constexpr bool disjunction(const T v) {
        return Disjunction<T>::eval(v);
    }

}  // namespace ferox

#endif  // FEROX_NUMBER_HPP
