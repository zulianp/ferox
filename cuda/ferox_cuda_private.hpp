#ifndef FEROX_CUDA_PRIVATE_HPP
#define FEROX_CUDA_PRIVATE_HPP

namespace ferox {
    void cuda_check(cudaError_t err, const char *file, const int line);

}  // namespace ferox

#define FEROX_CUDA_CHECK(macro_err_) ferox::cuda_check(macro_err_, __FILE__, __LINE__)

#endif  // FEROX_CUDA_PRIVATE_HPP
