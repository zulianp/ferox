#ifndef FEROX_MACROS_HPP
#define FEROX_MACROS_HPP

#include "ferox_Export.hpp"

#define FEROX_STR(x) #x
#define FEROX_STRINGIFY(x) STR(x)
#define FEROX_CONCATENATE(X, Y) X(Y)
#define FEROX_UNUSED(macro_expr_) (void)macro_expr_

#endif  // FEROX_MACROS_HPP