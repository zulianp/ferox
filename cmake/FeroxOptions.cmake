# FeroxOptions.cmake
option(FEROX_ENABLE_VC "Enable vectorized mode" ON)
option(FEROX_ENABLE_OPENMP "Enable openmp" OFF)
option(FEROX_ENABLE_MPI "Enable mpi" OFF)
option(FEROX_ENABLE_KOKKOS "Enable Kokkos" PFF)
option(FEROX_ENABLE_KOKKOS_KERNELS "Enable KokkosKernels" OFF)
option(FEROX_ENABLE_CUDA "Enable Cuda" OFF)
option(FEROX_ENABLE_MPI_SORT "Enable MPISort extension" OFF)

option(BUILD_SHARED_LIBS "Build shared libraries" ON)
option(FEROX_ENABLE_DEV_MODE
       "If not dev: disable if warnings are creating errors in your platform"
       ON)

if(FEROX_ENABLE_DEV_MODE)
    list(
        APPEND
        FEROX_DEV_FLAGS
        -Wall
        -Wextra
        -pedantic
        -Werror
        -Werror=enum-compare
        -Werror=delete-non-virtual-dtor
        -Werror=reorder
        -Werror=return-type)
endif()
