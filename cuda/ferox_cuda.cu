#include "ferox_cuda.hpp"

#include "ferox_cuda_private.hpp"

#include <cassert>
#include <thrust/device_vector.h>

__global__ void histogram (
    const ptrdiff_t n,
    const int sup,
    const int * const __restrict__ in,
    ptrdiff_t * const __restrict__ out )
{
    const ptrdiff_t entry = threadIdx.x + blockDim.x * (ptrdiff_t)blockIdx.x;

    if (entry >= n)
    return;

    const int value = in[entry];
    assert(value >= 0 && value < sup);

    atomicAdd(value + (unsigned long long *)out, 1ull);
}

namespace ferox {
    void cuda_info()
    {
        thrust::device_vector<double> vec;
        FEROX_CUDA_CHECK(cudaPeekAtLastError());
    }

    void cuda_check(cudaError_t stmt, const char *file, const int line) {
        cudaError_t retval = stmt;

        if (cudaSuccess != retval) {
            fprintf(stderr, "CUDA_CHECK FAILED: %s at %s:%d\n", cudaGetErrorString(retval), file, line);

            exit(EXIT_FAILURE);
        }
    }
}