#include "gtest/gtest.h"

#include "ferox.hpp"
using namespace ferox;

int main(int argc, char **argv) {
  Ferox::init(argc, argv);

  ::testing::InitGoogleTest(&argc, argv);
  int ok = RUN_ALL_TESTS();

  return Ferox::finalize() && ok;
}
