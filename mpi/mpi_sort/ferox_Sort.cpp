#include "ferox_Sort.hpp"
// #include "mpi-sort.h"
#include <cstdint>

extern "C" {

int MPI_Sort(const void *sendbuf,
             const int sendcount,
             MPI_Datatype datatype,
             const void *recvbuf,
             const int recvcount,
             MPI_Comm comm);

int MPI_Sort_bykey(const void *sendkeys,
                   const void *sendvals,
                   const int sendcount,
                   MPI_Datatype keytype,
                   MPI_Datatype valtype,
                   void *recvkeys,
                   void *recvvals,
                   const int recvcount,
                   MPI_Comm comm);
}

namespace ferox {

    template <>
    int Sort<int32_t>::apply(MPI_Comm comm, int nsend, int *sendbuf, int nrecv, int *recvbuf) {
        return MPI_Sort(sendbuf, nsend, MPI_INT32_T, recvbuf, nrecv, comm);
    }

}  // namespace ferox