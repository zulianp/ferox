#ifndef FEROX_API_HPP
#define FEROX_API_HPP

#include "ferox_Export.hpp"

namespace ferox {

    class FEROX_EXPORT Ferox {
    public:
        static void init(int argc, char *argv[]);
        static int finalize();
    };

}  // namespace ferox

#endif  // FEROX_API_HPP
