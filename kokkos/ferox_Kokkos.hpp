#ifndef FEROX_KOKKOS_HPP
#define FEROX_KOKKOS_HPP

#include "ferox_Export.hpp"

namespace ferox {
    const char* kokkos_info();
}

#endif  // FEROX_KOKKOS_HPP
