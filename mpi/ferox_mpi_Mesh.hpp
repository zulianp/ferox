#ifndef FEROX_MPI_MESH_HPP
#define FEROX_MPI_MESH_HPP

#include <mpi.h>

#include <cassert>

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

#include "ferox_Macros.hpp"

#define MPI_CHECK(macro_error_) ferox::mpi_check(macro_error_, #macro_error_, __FILE__, __LINE__)

namespace ferox {

    template <typename T>
    class MPIType {
        static_assert(sizeof(T) == 0, "IMPLEMENT ME");
    };

    template <>
    class MPIType<int> {
    public:
        inline static MPI_Datatype get() { return MPI_INT; }
    };

    template <>
    class MPIType<double> {
    public:
        inline static MPI_Datatype get() { return MPI_DOUBLE; }
    };

    template <>
    class MPIType<float> {
    public:
        inline static MPI_Datatype get() { return MPI_FLOAT; }
    };

    template <>
    class MPIType<long> {
    public:
        inline static MPI_Datatype get() { return MPI_LONG; }
    };

    void mpi_check(const int code, const char *expr, const char *file, const int line) {
        if (code != MPI_SUCCESS) {
            char msg[2048];
            int len = sizeof(msg);
            MPI_Error_string(code, msg, &len);

            fprintf(stderr, "MPI error:\n%s\n%s (%s:%d)\n", expr, msg, file, line);
            fflush(stderr);

            assert(false);

            MPI_Abort(MPI_COMM_WORLD, code);
        }
    }

    template <typename Scalar, typename Idx, typename...>
    class Mesh {
    public:
        class Range {
        public:
            ptrdiff_t begin{-1}, end{-1};
            constexpr ptrdiff_t count() const { return end - begin; }
            inline Range operator*(const ptrdiff_t scale) const { return {begin * scale, end * scale}; }
            inline bool intersects(const Range &other) const { return !(begin >= other.end || end < other.begin); }
            inline bool intersection(const Range &other, Range &result) {
                result = {std::max(begin, other.begin), std::min(end, other.end)};
                return !result.empty();
            }

            inline bool empty() const { return begin >= end; }

            void describe(std::ostream &os) const { os << begin << " " << end << "\n"; }
        };

        class Meta {
        public:
            int elem_type;
            int dim;
        };

        class CRSGraph {
        public:
            std::vector<Idx> row_ptr;
            std::vector<Idx> idx;
        };

        void read_header(const std::string &) {
            // FIXME
            meta_.elem_type = 8;
            meta_.dim = 3;
        }

        void read_raw(MPI_Comm comm, const std::string &dir) {
            comm_ = comm;

            read_header(dir + "/header.txt");
            auto path_elem = dir + "/elems.raw";

            ptrdiff_t n_idx = this->count_items<Idx>(path_elem);
            ptrdiff_t n_elem = n_idx / meta_.elem_type;

            assert(0 == n_idx % meta_.elem_type);

            int comm_rank, comm_size;
            MPI_Comm_rank(comm_, &comm_rank);
            MPI_Comm_size(comm_, &comm_size);

            elem_range_ = partition(comm_rank, comm_size, n_elem);
            read_array<Idx>(path_elem, elem_range_ * meta_.elem_type, elems_);

            read_points(dir);

            determine_node_ownership();
        }

        void read_points(const std::string &dir) {
            auto path_points = dir + "/points.raw";
            Idx start = 0, end = 0;
            if (!elems_.empty()) {
                start = elems_[0];
                end = elems_[0];

                for (auto e : elems_) {
                    start = std::min(start, e);
                    end = std::max(end, e);
                }

                end += 1;
            }

            node_range_with_ghosts_ = Range{start, end};
            Range range_points = node_range_with_ghosts_ * meta_.dim;

            read_array<Scalar>(path_points, range_points, points_);
        }

        template <typename T>
        ptrdiff_t count_items(const std::string &path) {
            ptrdiff_t retval;
            int rank;
            MPI_Comm_rank(comm_, &rank);

            if (0 == rank) {
                MPI_File f;
                MPI_CHECK(MPI_File_open(MPI_COMM_SELF, path.c_str(), MPI_MODE_RDONLY, MPI_INFO_NULL, &f));

                auto type = MPIType<T>::get();
                int type_size;
                MPI_CHECK(MPI_Type_size(type, &type_size));

                MPI_Offset file_size;
                MPI_CHECK(MPI_File_get_size(f, &file_size));
                assert(0 == file_size % type_size);

                retval = file_size / type_size;

                MPI_CHECK(MPI_File_close(&f));
            }

            MPI_CHECK(MPI_Bcast(&retval, 1, MPIType<ptrdiff_t>::get(), 0, comm_));
            return retval;
        }

        template <typename T>
        void read_array(const std::string &path, const Range &r, std::vector<T> &buff) {
            MPI_File f = NULL;
            auto type = MPIType<T>::get();

            MPI_CHECK(MPI_File_open(comm_, path.c_str(), MPI_MODE_RDONLY, MPI_INFO_NULL, &f));

            int type_size;
            MPI_CHECK(MPI_Type_size(type, &type_size));

            buff.resize(r.count());

            MPI_Status status;
            MPI_CHECK(MPI_File_read_at_all(f, r.begin * (ptrdiff_t)type_size, &buff[0], r.count(), type, &status));
            MPI_CHECK(MPI_File_close(&f));
        }

        void make_local_to_global() {
            local2global_ = elems_;

            std::sort(std::begin(local2global_), std::end(local2global_));
            auto last = std::unique(local2global_.begin(), local2global_.end());
            local2global_.erase(last, local2global_.end());

            assert(n_nodes() == static_cast<ptrdiff_t>(local2global_.size()));

            for (auto &e : elems_) {
                auto it = std::lower_bound(local2global_.begin(), local2global_.end(), e);

                if (it == local2global_.end() || *it != e) {
                    assert(false && "BUG!");
                    MPI_Abort(MPI_COMM_WORLD, -1);
                }

                auto idx = std::distance(local2global_.begin(), it);

                e = idx;
            }
        }

        void determine_node_ownership() {
            make_local_to_global();

            int comm_rank;
            MPI_Comm_rank(comm_, &comm_rank);

            int comm_size;
            MPI_Comm_size(comm_, &comm_size);

            ptrdiff_t nr[2] = {node_range_with_ghosts_.begin, node_range_with_ghosts_.end};

            int n_ranges = 2 * comm_size;
            std::vector<ptrdiff_t> all_ranges(n_ranges, 0);

            MPI_CHECK(
                MPI_Allgather(nr, 2, MPIType<ptrdiff_t>::get(), &all_ranges[0], 2, MPIType<ptrdiff_t>::get(), comm_));

            std::vector<ptrdiff_t> overlap(comm_size * 2, 0);

            connected_ranks_.clear();

            int n_intersection = 0;
            for (int i = 0; i < comm_size * 2; i += 2) {
                const int other_rank = i / 2;
                if (other_rank == comm_rank) continue;

                Range range_i = {all_ranges[i], all_ranges[i + 1]};
                Range range_intersection;

                if (node_range_with_ghosts_.intersection(range_i, range_intersection)) {
                    ++n_intersection;

                    auto it_lower_bound =
                        std::lower_bound(local2global_.begin(), local2global_.end(), range_intersection.begin);
                    auto it_upper_bound =
                        std::lower_bound(local2global_.begin(), local2global_.end(), range_intersection.end);

                    if (it_lower_bound == local2global_.end() || it_lower_bound == it_upper_bound) continue;

                    if (*it_lower_bound < range_intersection.begin) {
                        ++it_lower_bound;
                    }

                    overlap[i] = std::distance(local2global_.begin(), it_lower_bound);
                    overlap[i + 1] = overlap[i] + std::distance(it_lower_bound, it_upper_bound);

                    connected_ranks_.push_back(other_rank);
                }
            }

            int n_connected = static_cast<int>(connected_ranks_.size());

            std::vector<std::vector<Idx>> buffers(n_connected);
            std::vector<MPI_Request> requests(n_connected);

            for (int i = 0; i < n_connected; ++i) {
                int cr = connected_ranks_[i];
                ptrdiff_t start = overlap[cr * 2];
                ptrdiff_t end = overlap[cr * 2 + 1];
                ptrdiff_t n_candidates = end - start;

                auto &buff = buffers[i];

                buff.resize(n_candidates);

                if (cr > comm_rank) {
                    // send
                    for (ptrdiff_t k = 0; k < n_candidates; ++k) {
                        buff[k] = local2global_[start + k];
                    }

                    MPI_CHECK(
                        MPI_Isend(&buff[0], n_candidates, MPIType<Idx>::get(), cr, comm_rank, comm_, &requests[i]));

                } else {
                    // recv
                    MPI_CHECK(MPI_Irecv(&buff[0], n_candidates, MPIType<Idx>::get(), cr, cr, comm_, &requests[i]));
                }
            }

            MPI_CHECK(MPI_Waitall(n_connected, &requests[0], MPI_STATUSES_IGNORE));

            // We own everything
            owner_.resize(local2global_.size(), comm_rank);

            for (int i = 0; i < n_connected; ++i) {
                int cr = connected_ranks_[i];

                if (cr < comm_rank) {
                    // received
                    auto &buff = buffers[i];

                    for (auto v : buff) {
                        auto it = std::lower_bound(local2global_.begin(), local2global_.end(), v);
                        if (it == local2global_.end() || *it != v) continue;

                        auto idx = std::distance(local2global_.begin(), it);
                        // std::cout << idx << " " << v << std::endl;
                        owner_[idx] = cr;
                    }
                }
            }
        }

        static Range partition(const ptrdiff_t comm_rank, const ptrdiff_t comm_size, const ptrdiff_t num_elem) {
            const ptrdiff_t share = num_elem / comm_size;
            const ptrdiff_t remainder = num_elem % comm_size;

            const ptrdiff_t begin = share * comm_rank + (comm_rank < remainder ? comm_rank : remainder);
            const ptrdiff_t end = begin + share + (comm_rank < remainder);
            return {begin, end};
        }

        inline const Range &elem_range() const { return elem_range_; }

        void describe(std::ostream &os = std::cout) const {
            int comm_rank;
            MPI_Comm_rank(comm_, &comm_rank);

            int comm_size;
            MPI_Comm_size(comm_, &comm_size);

            for (int r = 0; r < comm_size; ++r) {
                MPI_Barrier(comm_);

                if (r == comm_rank) {
                    os << "--------------------------------------------\n";

                    os << "[" << comm_rank << "] ";
                    os << "connected_ranks: ";
                    for (auto cr : connected_ranks_) {
                        os << cr << " ";
                    }

                    os << "\n";
                    os << "elem_range: " << elem_range_.begin << " " << elem_range_.end << "\n";

                    ptrdiff_t ne = n_elems();
                    os << "n_elems " << ne << "\n";

                    for (ptrdiff_t i = 0; i < ne; ++i) {
                        ptrdiff_t nn = n_nodes(i);
                        auto e_nodes = nodes(i);
                        os << "lid: ";
                        for (ptrdiff_t k = 0; k < nn; ++k) {
                            os << e_nodes[k] << " ";
                        }

                        os << "\n";

                        os << "gid: ";
                        for (ptrdiff_t k = 0; k < nn; ++k) {
                            os << local2global_[e_nodes[k]] << " ";
                        }

                        os << "\n";
                    }

                    ptrdiff_t nn = n_nodes();

                    os << "n_nodes " << nn << "\n";

                    os << "box: " << node_range_with_ghosts_.begin << " " << node_range_with_ghosts_.end << "\n";

                    int dim = n_dims();
                    for (ptrdiff_t i = 0; i < nn; ++i) {
                        auto p = point(i);

                        os << i << ") gid: " << local2global_[i] << ", owner: " << owner_[i] << " coords: ";

                        for (int d = 0; d < dim; ++d) {
                            os << p[d] << " ";
                        }

                        os << "\n";
                    }

                    os << std::flush;
                }
            }

            MPI_Barrier(comm_);
        }

        inline int n_dims() const { return meta_.dim; }
        inline ptrdiff_t n_nodes() const { return points_.size() / meta_.dim; }
        inline ptrdiff_t n_elems() const { return elems_.size() / meta_.elem_type; }
        inline ptrdiff_t n_nodes(const ptrdiff_t elem_idx) const {
            FEROX_UNUSED(elem_idx);
            return meta_.elem_type;
        }

        inline const Idx *nodes(const ptrdiff_t elem_idx) const { return &elems_[elem_idx * meta_.elem_type]; }
        inline const Scalar *point(const ptrdiff_t node_idx) const { return &points_[node_idx * meta_.dim]; }

    private:
        MPI_Comm comm_;

        Meta meta_;
        Range elem_range_;
        std::vector<Idx> elems_;

        Range node_range_with_ghosts_;
        std::vector<Scalar> points_;
        std::vector<int> owner_;

        std::vector<Idx> local2global_;
        std::vector<int> connected_ranks_;

        CRSGraph ghost_graph_;
        std::vector<int> ghost_ranks_;
    };
}  // namespace ferox

#endif  // FEROX_MPI_MESH_HPP