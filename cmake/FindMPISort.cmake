# FindMPISort.cmake
find_library(
    MPISort_LIBRARIES
    NAMES mpi-sort
    PATHS ${MPISort_DIR} $ENV{MPISort_DIR}
    PATH_SUFFIXES lib bin build
    DOC "The MPISort_LIBRARIES library to link against")

find_path(
    MPISort_INCLUDE_DIRS
    NAMES "mpi-sort.h"
    HINTS ${MPISort_DIR} $ENV{MPISort_DIR}
    PATH_SUFFIXES include
    DOC "The MPISort_INCLUDE_DIRS path")

message(
    STATUS "MPISort include: ${MPISort_INCLUDE_DIRS} lib: ${MPISort_LIBRARIES}")

find_package_handle_standard_args(MPISort REQUIRED_VARS MPISort_LIBRARIES
                                                        MPISort_INCLUDE_DIRS)
