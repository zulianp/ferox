set(FEROX_TEST_DIR ${CMAKE_CURRENT_SOURCE_DIR}/tests)

list(APPEND TEST_MODULES .)

if(FEROX_HAVE_MPI)
    list(APPEND TEST_MODULES mpi)
endif()

if(FEROX_HAVE_MPI_SORT)
    list(APPEND TEST_MODULES mpi/mpi_sort)
    message("FEROX_HAVE_MPI_SORT")
endif()

if(FEROX_HAVE_CUDA)
    list(APPEND TEST_MODULES cuda)
endif()

find_project_files(${FEROX_TEST_DIR} "${TEST_MODULES}" UNIT_TESTS_HEADERS
                   UNIT_TESTS_SOURCES)

ferox_add_test(ferox_test "${UNIT_TESTS_SOURCES}")
